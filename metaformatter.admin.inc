<?php
/**
 * @file
 * FileField: Defines a CCK file field type.
 *
 * Uses content.module to store the fid and field specific metadata,
 * and Drupal's {files} table to store the actual file data.
 *
 * This file contains the forms and logic of the administration interface.
 */


/**
 * Form callback for the formatter preset overview.
 */
function metaformatter_presets_form(&$form_state) {
  $header = array(t('Formatter preset name'), t('Label'), t('Actions'));
  $rows = array();

  foreach (metaformatter_presets(true) as $preset) {
    $row = array();
    $row[] = check_plain($preset['preset_name']);
    $row[] = check_plain($preset['label']);
    $links = array();
    $links[] = l(t('Edit'), 'admin/build/metaformatter/'. $preset['preset_id']);
    if ($preset['preset_name'] != 'default') { // always keep this one at least
      $links[] = l(t('Delete'), 'admin/build/metaformatter/'. $preset['preset_id'] .'/delete');
    }
    $row[] = implode('&nbsp;&nbsp;&nbsp;&nbsp;', $links);
    $rows[] = $row;
  }
  $table = theme('table', $header, $rows);

  $form = array();
  $form['description'] = array(
    '#type' => 'markup',
    '#value' => t('The formatter presets that you can manage here will show up as CCK formatters for file fields on the "Display fields" tab of each content type.'),
  );
  $form['table'] = array(
    '#type' => 'markup',
    '#value' => $table,
  );
  return $form;
}

/**
 * Form callback for configuring a single formatter preset (which in itself
 * consists of a set of file formatters).
 */
function metaformatter_preset_settings_form(&$form_state, $preset = array()) {
  if (is_string($preset) && $preset == 'add') {
    $preset = array('preset_id' => 0, 'preset_name' => '', 'label' => '', 'formatters' => array());
  }
dvm($preset);
  if (empty($preset['preset_id'])) {
    $form['preset_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Preset name'),
      '#description' => t('Assign a unique name of this preset, consisting only of lowercase unaccented letters (a-z), numbers (0-9), and underscores. The length of the name is limited to no more than 32 letters. This name cannot be changed anymore after the formatter is saved.'),
      '#default_value' => '',
      '#size' => 40,
      '#maxlength' => 32,
    );
  }
  else {
    $form['preset_name'] = array(
      '#type' => 'value',
      '#value' => $preset['preset_name'],
    );
  }

  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#description' => t('The user visible name of this preset as it appears in the "Display fields" tab of content types, and in the "List" tab of the FileField file formatter preset management form.'),
    '#default_value' => $preset['label'],
    '#size' => 40,
    '#maxlength' => 255,
  );

  $file_formatter_info = array();

  // Hidden info for the validate and submit callbacks.
  $form['preset_id'] = array(
    '#type' => 'value',
    '#value' => $preset['preset_id'],
  );
  $form['file_formatter_info'] = array(
    '#type' => 'value',
    '#value' => $file_formatter_info,
  );

  $form['formatters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Formatters'),
    '#description' => t('Control how files may be displayed in the node view and other views for this field. If no formatters are enabled or are able to handle a file then that specific file will not be displayed. You can also reorder the formatters to specify their priority: the top-most enabled formatter always gets to display the files that it supports, whereas the bottom-most enabled formatter only gets to handle them if the file is not supported by any other other one.'),
    '#weight' => 5,
    '#tree' => true,
  );
  foreach ($preset['formatters'] as $key => $formatter) {
    $form['formatters'][$key] = array(
      '#type' => 'value',
      '#value' => $formatter,
    );
  }

  $form['buttons'] = array(
    '#weight' => 50,
  );
  $form['buttons']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save preset'),
    '#weight' => 5,
    '#validate' => array('metaformatter_preset_settings_validate'),
    '#submit' => array('metaformatter_preset_settings_save'),
  );

  if (!empty($preset['preset_id']) && $preset['preset_name'] != 'default') {
    $form['buttons']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#weight' => 10,
      '#submit' => array('metaformatter_preset_settings_delete'),
    );
  }
  return $form;
}

/**
 * Validation callback for a specific formatter preset configuration form.
 */
function metaformatter_preset_settings_validate($form, &$form_state) {
  // If we've got a new preset, make sure the preset id meets our expectations.
  if (empty($form_state['values']['preset_id'])) {
    $preset_name = $form_state['values']['preset_name'];

    if (empty($preset_name) || drupal_strlen($preset_name) > 32
        || !preg_match('!^[a-z0-9_]+$!', $preset_name)) {
      form_set_error('preset_name', t('You need to assign a unique name to this preset, consisting only of lowercase unaccented letters (a-z), numbers (0-9), and underscores. The length of the name is limited to no more than 32 letters.'));
    }

    $existing_preset = _metaformatter_preset_by_name($preset_name);
    if (!empty($existing_preset)) {
      form_set_error('preset_name', t('The preset name %presetname is already in use by another preset, please choose another name for this one.', array('%presetname' => $preset_name)));
    }
  }

  if (empty($form_state['values']['label'])) {
      form_set_error('preset_name', t('The "Label" field may not be empty.'));
  }

  // Let modules add their own formatter specific validations.
  $file_formatter_info = $form_state['values']['file_formatter_info'];

  foreach ($file_formatter_info as $file_formatter => $info) {
    $file_formatter_settings = isset($form_state['values']['formatters'][$file_formatter])
                                ? $form_state['values']['formatters'][$file_formatter]
                                : array();
    module_invoke(
      $info['module'], 'file_formatter_settings_'. $info['name'],
      'validate', $file_formatter_settings
    );
  }
}

/**
 * Save callback for a specific formatter preset configuration form.
 */
function metaformatter_preset_settings_save($form, &$form_state) {
  $preset = array(
    'preset_name'     => $form_state['values']['preset_name'],
    'label'           => $form_state['values']['label'],
    'formatters'      => $form_state['values']['formatters'],
  );
  if ($form_state['values']['preset_id'] != 0) {
    $preset['preset_id'] = $form_state['values']['preset_id'];
  }
  metaformatter_preset_save($preset);
  $form_state['redirect'] = array('admin/build/metaformatter');
}

/**
 * 'Delete' button submit callback, copied from node_form_delete_submit().
 */
function metaformatter_preset_settings_delete($form, &$form_state) {
  $destination = '';
  if (isset($_REQUEST['destination'])) {
    $destination = drupal_get_destination();
    unset($_REQUEST['destination']);
  }
  $form_state['redirect'] = array(
    'admin/build/metaformatter/'. $form_state['values']['preset_id'] .'/delete',
    $destination
  );
}

/**
 * 'Delete' confirm form callback.
 */
function metaformatter_preset_delete_form($form_state, $preset = array()) {
  if (empty($preset)) {
    drupal_set_message(t('The specified preset was not found.'), 'error');
    drupal_goto('admin/build/metaformatter');
  }
  $form = array();
  $form['preset_id'] = array('#type' => 'value', '#value' => $preset['preset_id']);

  return confirm_form(
    $form,
    t('Are you sure you want to delete the preset %presetname?',
      array('%presetname' => $preset['preset_name'])
    ),
    'admin/build/metaformatter',
    t('This action cannot be undone.'),
    t('Delete'), t('Cancel')
  );
}

/**
 * 'Delete' confirm form submit callback: Delete the preset.
 */
function metaformatter_preset_delete_form_submit($form, &$form_state) {
  $preset = metaformatter_preset($form_state['values']['preset_id']);
  metaformatter_preset_delete($preset);
  drupal_set_message(t('Preset %presetname deleted.', array(
    '%presetname' => $preset['preset_name']
  )));
  $form_state['redirect'] = 'admin/build/metaformatter';
}
